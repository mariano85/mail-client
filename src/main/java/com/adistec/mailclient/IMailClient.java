package com.adistec.mailclient;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface IMailClient {
	ResponseEntity<?> sendFeedback(String message, String appDesc, String appName, String userEmail, String userCompany, String userName, String subject) throws JsonProcessingException, URISyntaxException;
	ResponseEntity<?>sendRecovery(String userEmail, String hash, String appUrl, String subject) throws JsonProcessingException;
	ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, File template, Map<String, String> variables, String subject, Map<String, File> images) throws URISyntaxException, IOException;
	ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, File template, Map<String, String> variables, String subject, Map<String, File> images, String from) throws URISyntaxException, IOException;
	ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, File template, Map<String, String> variables, String subject, Map<String, File> images, String from, String[] copyTo) throws URISyntaxException, IOException;
}
