package com.adistec.mailclient.impl;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import com.adistec.mailclient.IMailClient;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MailClient implements IMailClient {
	private String mailSenderUrl;
	private HttpHeaders headers;
	/**
	 * 
	 * @param mailSenderUrl format: http://x.x.x.x:port/
	 */
	public MailClient(String mailSenderUrl) {
		this.mailSenderUrl = mailSenderUrl;
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
	    this.headers = headers;
	}
	
	private RestTemplate restTemplate;
    /**
     * 
     * @param sentToEmail
     * @param template 
     * @param variables key is the reference of the value in the template
     * @param subject
     * @param images key is the reference of the image in the template
     * @return
     * @throws URISyntaxException
     * @throws IOException
     */
	public ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, File template, Map<String, String> variables, String subject, Map<String, File> images) throws URISyntaxException, IOException {
        return sendEmailWithTemplate(sentToEmail, template, variables, subject, images, null, null, null);
    }
	
	/**
     * 
     * @param sentToEmail
     * @param template 
     * @param variables key is the reference of the value in the template
     * @param subject
     * @param images key is the reference of the image in the template
     * @return
     * @throws URISyntaxException
     * @throws IOException
     */
	public ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, File template, Map<String, String> variables, String subject, Map<String, File> images, String from) throws URISyntaxException, IOException {      
        return sendEmailWithTemplate(sentToEmail, template, variables, subject, images, from, null, null);
    }
	/**
     * 
     * @param sentToEmail
     * @param template 
     * @param variables key is the reference of the value in the template
     * @param subject
     * @param images key is the reference of the image in the template
     * @return
     * @throws URISyntaxException
     * @throws IOException
     */
	public ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, File template, Map<String, String> variables, String subject, Map<String, File> images, String from, String[] copyTo) throws URISyntaxException, IOException {
        return sendEmailWithTemplate(sentToEmail, template, variables, subject, images, from, copyTo, null);
    }
	/**
     * 
     * @param sentToEmail
     * @param template 
     * @param variables key is the reference of the value in the template
     * @param subject
     * @param images key is the reference of the image in the template
     * @return
     * @throws URISyntaxException
     * @throws IOException
     */
	public ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, File template, Map<String, String> variables, String subject, Map<String, File> images, String from, String[] copyTo, String [] cco) throws URISyntaxException, IOException {      
        HashMap<String, String> inlines = new HashMap<String, String>();
        if (images != null) {
        	for (Map.Entry<String, File> entry : images.entrySet()) {
            	byte[] readFileToByteArray = new byte[] {};
    			readFileToByteArray = FileUtils.readFileToByteArray(entry.getValue());
            	String encodedImage = Base64.getEncoder().encodeToString(readFileToByteArray);
            	inlines.put(entry.getKey(), encodedImage);
            }
        }
		String templateBytesString = new String(FileUtils.readFileToByteArray(template), "UTF-8");
		String inlinesString = new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).writeValueAsString(inlines);
        String templateVariables = new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).writeValueAsString(variables);
        String sentToEmailString = new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).writeValueAsString(sentToEmail);
        String copyToString = "";
        if (copyTo != null)
        	copyToString = new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).writeValueAsString(copyTo);
        String ccoString = "";
        if (cco != null)
        	ccoString = new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).writeValueAsString(cco);
        MultiValueMap<String, Object>  requestBody= new LinkedMultiValueMap<String, Object>();
        requestBody.add("sentToEmail", sentToEmailString);
        requestBody.add("inlines", inlinesString);
        requestBody.add("subject", subject);
        requestBody.add("template", templateBytesString);
        requestBody.add("templateVariables", templateVariables);
        if (from != null)
        	requestBody.add("from", from);
        if (!copyToString.isEmpty())
        	requestBody.add("copyTo", copyToString);
        if (!ccoString.isEmpty())
        	requestBody.add("cco", ccoString);
        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(requestBody, getHeaders());
        ResponseEntity<?> response = getRestTemplate().postForEntity(mailSenderUrl + "mail-sender/mail", request, Object.class);
        return response;
    }
	
	/**
	 * 
	 * @param message
	 * @param appDesc a descriptor for the app calling this method. Will be used to form description in feedback template. e.g: Customer Center
	 * @param appName Will be used to match the logo of the app in mail-sender. e.g customercenter.
	 * @param userEmail
	 * @param userCompany
	 * @param userName e.g "Juan Bosnic"
	 * @param subject
	 * @return
	 * @throws JsonProcessingException
	 */
    public ResponseEntity<?> sendFeedback(String message, String appDesc, String appName, String userEmail, String userCompany, String userName, String subject) throws JsonProcessingException {      
        HashMap<String, String> variables = new HashMap<String, String>();
        variables.put("message", message);
        variables.put("appDesc", appDesc);
        variables.put("userEmail", userEmail);
        variables.put("userCompany", userCompany);
        variables.put("userName", userName);
        String templateVariables = new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).writeValueAsString(variables);
        MultiValueMap<String, String>  requestBody= new LinkedMultiValueMap<String, String>();
        requestBody.add("appName", appName);
        requestBody.add("subject", subject);
        requestBody.add("templateVariables", templateVariables);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(requestBody, getHeaders());
        ResponseEntity<?> response = getRestTemplate().postForEntity(mailSenderUrl + "mail-sender/mail/feedback", request, Object.class);
        return response;
    }
    
    public ResponseEntity<?> sendRecovery(String userEmail, String hash, String appUrl, String subject) throws JsonProcessingException {
    	HashMap<String, String> variables = new HashMap<String, String>();
    	variables.put("username", userEmail);
    	variables.put("hash", hash);
    	variables.put("appUrl", appUrl);
    	String templateVariables = new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).writeValueAsString(variables);
    	MultiValueMap<String, String> requestBody= new LinkedMultiValueMap<String, String>();
    	requestBody.add("username", userEmail);
        requestBody.add("subject", subject);
        requestBody.add("templateVariables", templateVariables);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(requestBody, getHeaders());
        ResponseEntity<?> response = getRestTemplate().postForEntity(mailSenderUrl + "mail-sender/mail/recovery", request, Object.class);
        return response;
    }
	
	private RestTemplate getRestTemplate() {
		return restTemplate == null ? restTemplate = new RestTemplate() : restTemplate;
	}
	private HttpHeaders getHeaders() {
	     return this.headers;
	}

	public void setMailSenderUrl(String mailSenderUrl) {
		this.mailSenderUrl = mailSenderUrl;
	}
	
}