# README #

Cliente para envio de mails via mail-sender.

## Dependency ##

<dependency>
	<groupId>com.adistec</groupId>
	<artifactId>mail-client</artifactId>
	<version>1.1.0</version>
</dependency>

## Configuration ##

Preferably configure a bean inside a Configuration class.

MailConfiguration.java

```java
@Configuration
public class MailConfiguration {
	@Bean
	public MailClient mailClient() {
		MailClient client = new MailClient(mailSenderUrl); //Note this is the only way to instantiate MailClient. It requires mail-sender URL with format: http://x.x.x.x:port/
		return client;
	}
}
```
## Interface ##
```java
ResponseEntity<?> sendFeedback(String message, String appDesc, String appName, String userEmail, String userCompany, String userName, String subject) throws JsonProcessingException, URISyntaxException;
ResponseEntity<?>sendRecovery(String userEmail, String hash, String appUrl, String subject) throws JsonProcessingException;
ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, File template, Map<String, String> variables, String subject, Map<String, File> images) throws URISyntaxException, IOException;
ResponseEntity<?> sendEmailWithTemplate(String[] sentToEmail, File template, Map<String, String> variables, String subject, Map<String, File> images, String from) throws URISyntaxException, IOException;
```
## Use ## 

sendEmailWithTemplate(String[] sentToEmail, File template, Map<String, String> variables, String subject, Map<String, File> images, String from)

Autowire the MailClient bean.

```java
public ResponseEntity<?> exampleServiceMethod() throws URISyntaxException, IOException {
	HashMap<String, File> images = new HashMap<String, File>();
	String imagePath = "customercenter.png";
	File template = new ClassPathResource("template.html").getFile();
	File imageFile = new ClassPathResource(imagePath).getFile();
	images.put("image", logoFile); //Note key 'image' will be used to match the reference inside the template.
	HashMap<String, String> variables = new HashMap<String, String>(); //Note key will be used to match the reference inside the template.
	variables.put("var", "some value");
	return mailClient.sendEmailWithTemplate(new String[] {"example@host.com"}, template, variables, "Sample subject", images, "exmaple2@host.com"); //Note from must be valid from for account devsend@adistec.com. e.g: no-reply@adistec.com
}
```
